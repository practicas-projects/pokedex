const Pokedex = (()=>{
    let publicFunction = {};
    const server = document.getElementById("server").value;
    publicFunction.init = () => {
             fetch('Api/getPokemons/',{
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                      },
                      body: JSON.stringify(this.data)
                  })
                .then((response) => { 
                    return response.json();
                })
                .then((res) => {
                    const contenedorPokemons = document.getElementById("contenedor_pokemon");
                    res.results.forEach(element => {
                        let id = element.url.split('/')[6];
                        contenedorPokemons.innerHTML +=`<div class="card" style="width: 10rem;" id="${element.name}">
                        <img id=${element.name} src="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png" class="card-img-top pokemon" >
                        <div class="card-footer bg-danger">
                        <small class="text-white">${element.name}</small>
                        </div>
                        </div>`;

                    });
                    document.getElementById("contenedor_pokemon").addEventListener('click', function (e) {
                        const target = e.target;
                        if(target){
                            buscarPokemon(target.id) 
                        }
                    });
                    const elements = document.getElementsByClassName("classname");
                    Array.from(elements).forEach(function(element) {
                        element.addEventListener('click',function(){
                            console.log("LLEGUE")
                        });
                      });
                })
                .catch(function (error) {
                    alert(error);
                });
          }

    const buscarPokemon = (name)=>{
        const modalPokemon = document.getElementById("pokemonModal");
        const myModal = new bootstrap.Modal(modalPokemon)
        
        fetch('https://pokeapi.co/api/v2/pokemon/'+name)
        .then((response) => { 
            return response.json();
        })
        .then((res) => {
            
            const nombre = document.getElementById("nombre");
            const habilidades = document.getElementById("habilidades");
            const movimientos = document.getElementById("movimientos");
            const photoPokemon = document.getElementById("imgPokemon");
            habilidades.innerHTML ="";
            movimientos.innerHTML ="";
            photoPokemon.innerHTML = "";
            addElements('<span class="badge text-bg-primary">****</span>',habilidades,res.abilities,'ability')
            addElements('<span class="badge text-bg-success">****</span>',movimientos,res.moves,'move')
            nombre.innerHTML = name;
            photoPokemon.innerHTML =`<img  src="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${res.id}.png"></img> `;
            
        }
        
        );
        myModal.show()

    } 
    const addElements=(elemento,$this,data,id)=>{
        data.forEach(x => {
            $this.innerHTML += elemento.replace("****",x[id].name);
        });

    }
    return publicFunction;
})();

window.addEventListener("load", function(event) {
    Pokedex.init();
});

