<?php
require_once __DIR__ . "\BaseController.php";

class Api extends BaseController
{

    public function getPokemons(){
        $ch = curl_init();
        // Establecer URL y otras opciones apropiadas
        curl_setopt($ch, CURLOPT_URL, "https://pokeapi.co/api/v2/pokemon/?limit=204");
        curl_setopt($ch, CURLOPT_HEADER, 0);

        // Capturar la URL y pasarla al navegador
        curl_exec($ch);

        // Cerrar el recurso cURL y liberar recursos del sistema
        curl_close($ch);
        //$this->sendOutput(json_encode($response),array('Content-Type: application/json', 'HTTP/1.1 200 OK'));
    }
}