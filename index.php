<?php

define("PROJECT_ROOT_PATH", __DIR__ );
require_once PROJECT_ROOT_PATH . "/Controller/Api.php";


$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri = explode( '/', $uri );

if($uri[2] == 'Pokedex'){
    $view = file_get_contents("view/Pokedex.html");
    //$_SERVER['SERVER_ADDR']
    echo str_replace('******', $_SERVER['HTTP_HOST'], $view);
    exit(); 
}

if ((isset($uri[2]) && $uri[2] != 'Api') || !isset($uri[3])) {
    header("HTTP/1.1 404 Not Found");
    exit();
}

$objFeedController = new Api();

$strMethodName = $uri[3];
$objFeedController->{$strMethodName}();
?>